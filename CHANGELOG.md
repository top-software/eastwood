### 2.2.1

- Fix: do no longer abort when module could not be parsed when checking dcl documentation
       as this causes the language server to crash.

### 2.2.0

- Feature: the validity of .dcl documentation syntax
           (see: https://gitlab.com/clean-and-itasks/clean-platform/-/blob/master/doc/DOCUMENTATION.md)
           is now automatically checked when using the Eastwood language server or linter.

#### 2.1.3

- Fix: prevent `bad file descriptor` crashes when using `eastwood-cls` for some time.

#### 2.1.2

- Fix: prevent linter crashes for expressions with types.

#### 2.1.1

- Fix: prevent deadlocks while reading large compiler outputs.

## 2.1.0

- Feature: support syntax introduced with `base-compiler` `4.0`.

#### 2.0.0

- Remove: Removed root directory from default search paths to improve
          performance for finding definitions/declarations in case there
          are subdirectories which do not contain relevant source files.
          (e.g when including repositories in repositories to build with local dependency).

#### 1.1.3

- Fix: Add timeout for grep in case it takes too long to return all results. Do no longer allow to go to the declaration
       or definition of import as grep would not return.
#### 1.1.2

- Fix: show stdout of compiler if compiler crashes, compiler sometimes prints errors to stdout
  instead of stderr. Therefore, the errors would not end up being shown to the user by Eastwood.

#### 1.1.1
- Fix: incorrect character range for the CAF linter warnings.

### 1.1.0
- Feature: the language server now includes linter output.

#### 1.0.2
- Fix: make compiler option in Eastwood.yml work again.

#### 1.0.1
- Fix: include fixes of the YAML parser (see yaml v1.1.2).

## 1.0.0
- Initial version using nitrile semantic versioning.
