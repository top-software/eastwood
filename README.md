# Eastwood linter and language server

Eastwood is a linter and language server for the functional programming language [Clean][].

Neovim and VS Code are the currently supported IDEs. Other IDEs will need to be configured manually.

## The project configuration file (Eastwood.yml)

Eastwood requires a project configuration file `Eastwood.yml` in the current working directory.
This is a YAML file with the following fields:

- `compiler`: compiler executable (usually `cocl`)
- `paths`: Relative or absolute paths in which included modules are located

An example Eastwood.yml file:

```
compiler: cocl
paths: []
```

It is required to specify a compiler.

It is required to specify a YAML list for the paths key (use "paths: []" for an empty list).

## Usage in Neovim

The configuration below assumes you have neovim 0.5 or greater with lsp-config installed. Simply add the following to
your vimrc:

```lua
lua << EOF
  local lspconfig = require'lspconfig'
  local util = require'lspconfig/util'
  local configs = require'lspconfig/configs'
  if not lspconfig.eastwood then
    configs.eastwood = {
      default_config = {
        cmd = {'eastwood-cls'};
        filetypes = {'clean'};
        root_dir = util.root_pattern 'Eastwood.yml',
        settings = {};
      };
    }
  end
  lspconfig.eastwood.setup{}
EOF
```

## Usage in VS Code

Use the [Eastwood extension for VS Code](https://gitlab.com/top-software/eastwood-vs-code).

## Installation

Binaries are available at the [Clean package registry](https://clean-lang.org/pkg/eastwood/).
Eastwood should be installed using [Nitrile][]:

```
nitrile global install eastwood
```

If Eastwood is used as linter within a Nitrile project, it should be added as
[dependency with `Test` scope](https://clean-and-itasks.gitlab.io/nitrile/nitrile-yml/reference/#dependenciesscope).

## Building

Eastwood can be built and tested using [Nitrile][].
The sources can be compiled as follows:

```bash
nitrile build
```

## Running tests

The tests can be run as follows:

```bash
nitrile test
```

## Documentation

Further documentation can be found [here](https://gitlab.com/top-software/eastwood/-/tree/main/docs).

## Copyright &amp; License

Eastwood is copyright &copy; 2021 TOP Software Technology B.V.

This software is licensed under the GPL 3.0 or later license. For details, see the
[LICENSE](/LICENSE) file.

[Clean]: https://clean-lang.org
[Nitrile]: https://clean-lang.org/about.html#install