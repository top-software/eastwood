module ok

/**
 * This is a test program which should compile without warnings and errors.
 */

// imports from libraries
import StdEnv
import Data.Func
// import from other path provided in project file
import someLib.TestModule

// test if dynamics are supported
dyn :: Dynamic
dyn = dynamic ()

Start = ()
