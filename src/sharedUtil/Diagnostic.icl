implementation module Diagnostic

import Data.GenEq
import StdEnv
from Range import :: CharacterRange, :: Position, :: Range, instance == (Range t), instance == Position


instance == Diagnostic derive gEq

// We cannot compare these as the type is extensible.
instance == DiagnosticSource where
	(==) _ _ = True

instance == DiagnosticSeverity derive gEq
