module EastwoodLint

import StdEnv
import StdMaybe

import Control.Applicative
from Control.Monad import >>=, >>|, class Monad(bind), mapM, mapM_
from Data.Error import instance <*> (MaybeError a), instance Functor (MaybeError a), instance Monad (MaybeError a)
from Data.Error import instance pure (MaybeError a)
import qualified Data.Error
from Data.Func import $
from Data.Functor import <$>, class Functor(fmap), instance Functor []
import Data.GenDefault
from Data.List import intersperse
from Data.Maybe import maybe
import Data.Tuple
from System.CommandLine import getCommandLine, setReturnCode
from System.File import :: FileError, instance toString FileError, fileExists
from System.FilePath import dropDirectory, :: FilePath, takeExtension
import System.Options
import System.SysCall
import Text
import Text.GenParse

import Linter
import Util.Configuration
import Diagnostic
from Pass.BasicValueCAFs import :: BasicValueCAFsConfiguration{..}, BasicValueCAFsPass
from Pass.TrailingWhitespace import :: TrailingWhitespaceConfiguration{..}, TrailingWhitespacePass
import Range

RED :== "\x1b[31m"
YELLOW :== "\x1b[33m"
BLUE :== "\x1b[34m"
CYAN :== "\x1b[36m"
CLEAR :== "\x1b[0m"

derive gDefault Range

invalidOptions :: !Options -> Bool
invalidOptions opts = isNone opts.file

usage :: String
usage = " [options] FILE"

optionDesciption :: Option Options
optionDesciption = WithHelp True $ Options
	[ Flag "--no-color"
		(\opts -> 'Data.Error'.Ok {opts & color = False})
		"Do not use ANSI escape codes for colored output"
	, Flag "--Werror"
		(\opts -> 'Data.Error'.Ok {opts & werror = True})
		"Treat all warnings as errors"
	, Shorthand "-l" "--lines" $ Option
		"--lines"
		(\ls opts -> (\ls -> {opts & lines = ls}) <$> (mapM parseLineRange $ split "," ls))
		"LINE_RANGES"
		"Line ranges that should be considered (e.g. \"1-2,5-\")"
	, Operand True
		(\fp opts -> ?Just $ 'Data.Error'.Ok {opts & file = ?Just fp})
		"FILE"
		"The file on which the linter should be run"
	]
where
	parseLineRange :: String -> 'Data.Error'.MaybeError [String] LineRange
	parseLineRange str = case split "-" str of
		[] -> 'Data.Error'.Error ["No line range given"]
		[_] -> 'Data.Error'.Error ["No \"-\" found in line range \"" <+ str <+ "\""]
		[start, end] -> parseLineRange` start end
		_ -> 'Data.Error'.Error ["Multiple \"-\" found in line range \"" <+ str <+ "\""]
	where
		parseLineRange` :: !String !String -> 'Data.Error'.MaybeError [String] LineRange
		parseLineRange` "" "" = 'Data.Error'.Ok { Range | start = ?None, end = ?None }
		parseLineRange` "" end = case parseString end of
			?None -> 'Data.Error'.Error ["Could not parse \"" <+ end <+ "\" in line range"]
			?Just end -> 'Data.Error'.Ok { Range | start = ?None, end = ?Just end }
		parseLineRange` start "" = case parseString start of
			?None -> 'Data.Error'.Error ["Could not parse \"" <+ start <+ "\" in line range"]
			?Just start -> 'Data.Error'.Ok { Range | start = ?Just start, end = ?None }
		parseLineRange` start end = case (parseString start, parseString end) of
			(?None, _) -> 'Data.Error'.Error ["Could not parse \"" <+ start <+ "\" in line range"]
			(_, ?None) -> 'Data.Error'.Error ["Could not parse \"" <+ end <+ "\" in line range"]
			(?Just start, ?Just end) -> 'Data.Error'.Ok { Range | start = ?Just start, end = ?Just end }

showEastwoodDiagnostic :: !Options !Diagnostic -> String
showEastwoodDiagnostic {color, file} d
	# file = maybe "" (\f -> dropDirectory f +++ ":") file
	= concat
		[ if color (colorCode d.Diagnostic.severity) ""
		, toString d.Diagnostic.severity
		, ": "
		, if color CLEAR ""
		, toString d.source
		, "."
		, toString d.dCode
		, ": "
		, file
		, toString d.range
		, ": "
		, toString d.message
		]
where
	colorCode :: !DiagnosticSeverity -> String
	colorCode Error = RED
	colorCode Warning = YELLOW
	colorCode Information = BLUE
	colorCode Hint = CYAN

instance toString DiagnosticSeverity
where
	toString Error = "Error"
	toString Warning = "Warning"
	toString Information = "Info"
	toString Hint = "Hint"

instance toString DiagnosticSource
where
	toString BasicValueCAFsPass = "basic-value-caf"
	toString TrailingWhitespacePass = "whitespace"
	toString _ = "MISSING_TO_STRING_FOR_SOURCE"

instance toString (Range t) | toString t
where
	toString { Range | start, end } = concat3 (toString start) "-" (toString end)

instance toString Position
where
	// line + 1 because internally we use 0-based line counting.
	toString { Position | line, character } = concat3 (toString $ line + 1) "," (toString character)

Start :: !*World -> *World
Start world
	# ([prog:args], world) = getCommandLine world
	# opts = parseOptions optionDesciption args gDefault{|*|}
	| 'Data.Error'.isError opts
		= exit (join "\n" $ 'Data.Error'.fromError opts) world
	# opts = 'Data.Error'.fromOk opts
	| invalidOptions opts
		= exit (concat5 "Usage: " prog usage "\n\n" (showHelpText (helpText optionDesciption))) world
	# fileName = fromJust opts.file
	# (diagnostics, world) = runPassesFile (createConfiguration opts) fileName world
	= case diagnostics of
		'Data.Error'.Error fileError
			# (out, world) = stdio world
			# out = out <<< toString fileError <<< "\n"
			# (_, world) = fclose out world
			= setReturnCode 1 world
		'Data.Error'.Ok diagnostics
			#! diagnostics = handleWError opts.werror diagnostics
			# world = showEastwoodDiagnostics opts diagnostics world
			= returnCode diagnostics world
where
	exit :: String *World -> *World
	exit error w = snd $ fclose (stderr <<< error <<< "\n") $ setReturnCode 1 w

	handleWError :: !Bool ![Diagnostic] -> [Diagnostic]
	handleWError False ds = ds
	handleWError True ds = map asError ds
	where
		asError :: !Diagnostic -> Diagnostic
		asError d = {Diagnostic | d & severity = Error}

	returnCode :: ![Diagnostic] !*World -> *World
	returnCode ds world = if (any isError ds) (setReturnCode 1 world) world
	where
		isError :: !Diagnostic -> Bool
		isError {Diagnostic | severity = Error} = True
		isError _ = False

showEastwoodDiagnostics :: !Options ![Diagnostic] !*World -> *World
showEastwoodDiagnostics opts diagnostics world
	# (out, world) = stdio world
	# out = foldl (\out diag = out <<< showEastwoodDiagnostic opts diag <<< "\n") out diagnostics
	# (_, world) = fclose out world
	= world
