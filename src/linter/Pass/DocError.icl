implementation module Pass.DocError

import Clean.Doc
import Clean.Parse
from Clean.Parse.Comments import scanComments, :: CleanComment {content, line}
import Data.Bifunctor
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.Tuple
import StdEnv
import System.File
import System.FilePath

from Diagnostic import :: DiagnosticSource, :: Diagnostic {..}, :: DiagnosticSeverity
import qualified Diagnostic as Diagnostic
import Range

runPass :: !FilePath !*World -> (![Diagnostic], !*World)
runPass pathToDcl w
	| takeExtension pathToDcl <> "dcl" = ([], w)
	# (fileToLintExists, w) = fileExists pathToDcl w
	| not fileToLintExists = ([], w)
	| otherwise = appFst (flatten o fmap docErrorsToDiagnostics) $ checkForDocErrors pathToDcl w

instance toString ParseWarning
where
	toString (UnknownField f)   = "Doc warning: unknown field '" +++ f +++ "'"
	toString (IllegalField f)   = "Doc warning: illegal field '" +++ f +++ "'"
	toString NoDescription      = "Doc warning: missing description"
	toString UsedReturn         = "Doc warning: @return is deprecated, use @result"
	toString (UnparsableType t) = "Doc warning: could not parse type '" +++ t +++ "'"

instance toString ParseError
where
	toString (MissingAsterisk l) = "Doc error: missing leading asterisk in '" +++ l +++ "'"
	toString (MissingField f)    = "Doc error: required field '" +++ f +++ "' was missing"
	toString (UnknownError e)    = "Doc error: " +++ e
	toString InternalNoDataError = "Doc error: internal parsing error"

/**
 * Checks a .dcl module for documentation errors.
 * (see https://gitlab.com/clean-and-itasks/clean-platform/-/blob/master/doc/DOCUMENTATION.md).
 *
 * @param The path to the .dcl module.
 * @param World.
 * @return Either the warnings or error which parsing the dcl documentation yielded,
 *         alongside the line of the comment for which the error occurred.
 * @result World.
 */
checkForDocErrors :: !FilePath !*World -> (![Either (ParseError, Int) [(ParseWarning, Int)]], !*World)
checkForDocErrors pathToDcl w
	// Find properties
	#! (comments,w) = scanComments pathToDcl w
	#! (dcl,w) = readModule pathToDcl w
	#! commentsContentWithLine = case dcl of
		Error _ = []
		Ok (dcl,_) = case comments of
			Error err = []
			Ok comments = (\comment = (comment.content, comment.CleanComment.line)) <$> comments
	# errorsAndWarnings =
		((\(comment, line) =
			// This bifmap just inserts the line for the warnings/error.
			bifmap ((flip tuple) line) (appSnd (\warnings = [(parseWarning, line) \\ parseWarning <- warnings]))
				(parseDoc comment)) <$>
			commentsContentWithLine :: ([Either (ParseError, Int) (FunctionDoc, [(ParseWarning, Int)])]))
	= (fmap snd <$> errorsAndWarnings, w)

/**
 * Converts documentation error/warnings (retrieved by using e.g `checkForDocErrors`) to `Diagnostic`s.
 *
 * @param The documentation error/warnings alongside the line number of the comment for which the error/warning occured.
 * @result The diagnostic messages needed to inform the client about the warnings/error.
 */
docErrorsToDiagnostics :: !(Either (ParseError,Int) [(ParseWarning, Int)]) -> [Diagnostic]
docErrorsToDiagnostics errorOrWarnings = case errorOrWarnings of
	Right warnings = errorStringToDiagnostic 'Diagnostic'.Warning o appFst toString <$> warnings
	Left (error, line) = [errorStringToDiagnostic 'Diagnostic'.Error (toString error, line)]

errorStringToDiagnostic :: !DiagnosticSeverity !(!String, !Int) -> Diagnostic
errorStringToDiagnostic severity (error, line) =
	{range = singleLineRange (dec line) 0 0, severity = severity, dCode = DOC_ERROR_CODE
	, source = DocErrorChecker, message = error}

DOC_ERROR_CODE :== 0

:: DiagnosticSource | DocErrorChecker
