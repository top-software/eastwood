definition module Pass.TrailingWhitespace

/**
 * This pass will only check if every line in the file doesn't end with a whitespace character and produces a diagnostic
 * for each instance.
 */

from Util.Configuration import :: LineRange
from Diagnostic import :: Diagnostic, :: DiagnosticSeverity, :: DiagnosticSource
from Range import :: Range

/**
 * Add our source to the list of diagnostic sources. See Diagnostic for more information.
 */
:: DiagnosticSource | TrailingWhitespacePass

/**
 * The configuration for the TrailingWhitespace pass. Diagnostics are generated with the specified severity.
 */
:: TrailingWhitespaceConfiguration =
	{ severity :: !?DiagnosticSeverity
	}

/**
 * Runs the whitespace checking pass.
 */
runPass :: !TrailingWhitespaceConfiguration ![String] -> [Diagnostic]
