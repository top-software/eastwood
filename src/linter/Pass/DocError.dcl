definition module Pass.DocError

from System.FilePath import :: FilePath

from Diagnostic import :: Diagnostic

runPass :: !FilePath !*World -> (![Diagnostic], !*World)
