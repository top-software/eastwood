implementation module Linter

import StdEnv

import Clean.Parse
import Data.Error
from Data.Func import $, mapSt
from Data.Functor import class Functor(..), <$>, instance Functor ?
from Data.GenDefault import generic gDefault
import Data.Maybe
import Data.Tuple
from System.File import :: FileError, instance toString FileError, readFile
from System.FilePath import :: FilePath
from Text import class Text(split), instance Text String

import Util.Configuration
from Diagnostic import :: Diagnostic(..), :: DiagnosticSource, :: DiagnosticSeverity (Warning)
from Pass.BasicValueCAFs import :: BasicValueCAFsConfiguration {..}
from Pass.TrailingWhitespace import :: TrailingWhitespaceConfiguration {..}
import qualified Pass.BasicValueCAFs
import qualified Pass.DocError
import qualified Pass.TrailingWhitespace

runPassesFile :: !Configuration !FilePath !*World -> (MaybeError String [Diagnostic], !*World)
runPassesFile configuration filePath world
	#! (mbContents, world) = readFile filePath world
	| isError mbContents = (Error (toString (fromError mbContents)), world)
	#! (mbRes, world) = readModule filePath world
	# parsedModule = parsedModuleFromResult mbRes
	= (appFst Ok $ runPasses configuration filePath (splitLines (fromOk mbContents)) parsedModule world)
where
	parsedModuleFromResult ::
		!(MaybeError (String, ?(ParsedModule, HashTable)) (ParsedModule, HashTable)) -> ?ParsedModule
	parsedModuleFromResult (Error (_, pm)) = fst <$> pm
	parsedModuleFromResult (Ok (pm, _)) = ?Just pm

/**
 * @param The configuration.
 * @param The lines of the file.
 * @param Optionally, the parsed syntax tree. When not given, some passes in the configuration may be ignored.
 */
runPasses :: !Configuration !FilePath ![String] !(?ParsedModule) !*World -> (![Diagnostic], !*World)
runPasses {lineRanges, passes} filePath contents mbParsedModule world
	# (diagnostics, world) = mapSt runPasses` passes world
	= (flatten $ map (filterDiagnostics lineRanges) diagnostics, world)
where
	runPasses` :: !PassConfiguration !*World -> (![Diagnostic], !*World)
	runPasses` (BasicValueCAFsConfiguration passConfig) world =
		(withParsedModule 'Pass.BasicValueCAFs'.runPass passConfig, world)
	runPasses` (TrailingWhitespaceConfiguration passConfig) world =
		(withLines 'Pass.TrailingWhitespace'.runPass passConfig, world)
	runPasses` DocErrorConfiguration world = 'Pass.DocError'.runPass filePath world

	// helper functions for runPasses`
	withLines run config = run config contents
	withParsedModule run config = maybe [] (run config contents) mbParsedModule

	filterDiagnostics :: ![LineRange] ![Diagnostic] -> [Diagnostic]
	filterDiagnostics _ [] = []
	filterDiagnostics [] _ = []
	filterDiagnostics lrx=:[lr:lrs] dsx=:[d=:{range}:ds]
		| inLineRange lr range = [d:filterDiagnostics lrx ds]
		| afterLineRange lr range = filterDiagnostics lrs dsx
		| otherwise = filterDiagnostics lrx ds

splitLines :: !String -> [String]
splitLines string = splitLines` string 0
where
	splitLines` :: !String !Int -> [String]
	splitLines` string i = case findNewLine string i of
		?None = if (size string == i) [] [string % (i, size string - 1)]
		?Just (start, end) = [string % (i, start - 1) : splitLines` string (end + 1)]
	where
		// Results in the index of the first newline character and the index of the last newline character
		// For \n these will always be the same, for \r\n these will be different by 1
		findNewLine :: !String !Int -> ?(Int, Int)
		findNewLine str i
			| i >= size str = ?None
			| str.[i] == '\n' = ?Just (i, i)
			| i >= (size str) - 1 = ?None
			| str.[i] == '\r' && str.[i + 1] == '\n' = ?Just (i, i + 1)
			| otherwise = findNewLine str (i + 1)

createConfiguration :: !Options -> Configuration
createConfiguration {lines} =
	{ defaultConfiguration
	& lineRanges = lines
	}

defaultConfiguration :: Configuration
defaultConfiguration =
	{ Configuration
	| lineRanges = [{ Range | start = ?None, end = ?None }]
	, passes =
		[ BasicValueCAFsConfiguration defaultBasicValueCAFsConfiguration
		, TrailingWhitespaceConfiguration defaultTrailingWhitespaceConfiguration
		, DocErrorConfiguration
		]
	}
where
	defaultBasicValueCAFsConfiguration :: BasicValueCAFsConfiguration
	defaultBasicValueCAFsConfiguration =
		{ BasicValueCAFsConfiguration
		| severity = ?Just Warning
		}

	defaultTrailingWhitespaceConfiguration :: TrailingWhitespaceConfiguration
	defaultTrailingWhitespaceConfiguration =
		{ TrailingWhitespaceConfiguration
		| severity = ?Just Warning
		}

gDefault{|Options|} =
	{ Options
	| color = True
	, werror = False
	, lines = defaultConfiguration.lineRanges
	, file = ?None
	}
