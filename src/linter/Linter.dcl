definition module Linter

/**
 * Eastwood is a library that can be used to analyze Clean source code and is intended for use in a linter and language
 * server.
 */

from Data.Error import :: MaybeError
from Data.GenDefault import generic gDefault
from System.File import :: FileError
from System.FilePath import :: FilePath

from Range import :: LineRange, :: Range
from Util.Configuration import :: Configuration
from Diagnostic import :: Diagnostic

:: Options =
	{ color :: !Bool //* Use colored output for diagnostics severity.
	, werror :: !Bool //* Treat warnings as errors.
	, lines :: ![LineRange] //* The lines in the file to apply the linter to.
	, file :: !?FilePath //* The file to apply the linter to. If ?None, no diagnostics will be provided.
	}

/**
 * Runs Eastwood on the contents of the specific FilePath.
 *
 * @param The Eastwood configuration
 * @param The file for which Eastwood should generate diagnostics
 * @param The world
 * @result The Diagnostics or an error string in case reading the file went wrong
 * @result The world
 */
runPassesFile :: !Configuration !FilePath !*World -> (MaybeError String [Diagnostic], !*World)

/**
 * Create a custom eastwood configuration using the provided options.
 */
createConfiguration :: !Options -> Configuration

/**
 *  The default configuration which runs all passes.
 */
defaultConfiguration :: Configuration

derive gDefault Options
