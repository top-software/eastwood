definition module GotoUtil

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath
from Text.GenJSON import :: JSONNode
from Text.Unicode.UChar import :: UChar

from LSP.BasicTypes import :: UInt
from LSP.Internal.Serialize import generic gLSPJSONDecode
from LSP.Location import :: Location
from LSP.Position import :: Position
from LSP.RequestId import :: RequestId
from LSP.RequestMessage import :: RequestMessage
from LSP.ResponseMessage import :: ResponseMessage
from LSP.TextDocumentIdentifier import :: TextDocumentIdentifier

from Config import :: EastwoodState

//* Context information for a request concerning a specific line and character.
:: GotoPrerequisites =
	{ line :: !String //* The line number from which go to request was made.
	, charNr :: !UInt //* The char number within the line from which the go to request was made.
	, searchPaths :: ![FilePath] //* The paths to search in given a search request.
	}

//* Parameters for goto declaration or goto definition requests.
:: GotoDeclarationOrDefinitionParams =
	{ textDocument :: !TextDocumentIdentifier //* The document.
	, position :: !Position //* The position within the document.
	}

:: SearchType = Definition | Declaration | SingleFile !FilePath

derive gLSPJSONDecode GotoDeclarationOrDefinitionParams

/**
 * Retrieves the prerequisites for being able to go to definitions/declarations.
 *
 * @param The go to definition/declaration request.
 * @param The eastwood state.
 * @param World.
 * @result ResponseMessage in case of error, the prerequisites in case of success.
 * @result World.
 */
gotoPrerequisitesFor
	:: !RequestMessage !EastwoodState !*World -> (MaybeError ResponseMessage GotoPrerequisites, !*World)

/**
 * This function is used to check the surrounding lines included in a grep result ending on a provided stop symbol
 * A predicate which much hold before a stop symbol is found may be provided.
 *
 * @param The predicate that should hold for the characters that are found before finding a stop symbol.
 * @param The stop symbols which should lead to short circuiting as the symbol which is searched for is found.
 * @param Whether the line should be reversed (check for stop symbol reading from end of line to front).
 * @param The lines to check.
 * @result The resulting lines that pass the filter along with their positon relative to the actual result.
 */
filterSurroundingLinesForPredUntilStopSymbol :: !(Char -> Bool) ![!Char] !Bool ![String] -> [(Int, String)]

/**
 * Performs and processes grep results for search terms.
 *
 * @param The type of the search: any declaration module (.dcl), any definition (.icl) or single file.
 * @param Optionally, a file to exclude when searching.
 * @param The regex search term for which results should be found.
 * @param The paths to search in
 * @param The id of the request
 * @param The eastwood state
 * @param The non regex search term, used for logging what the user searched if an error occurred..
 * @param World.
 * @result Error: an error response to send back to the client.
 * @result Success: The filenames and line numbers of the lines that match the search term.
 * @result World.
 */
grepResultsForSearchTerm ::
	!SearchType !(?FilePath) !String ![FilePath] ![String] !(String -> [(String, Int)]) !RequestId !String !*World
	-> (!MaybeError ResponseMessage [(String, Int)], !*World)

//* Transforms the grep stdout output to a list of filepaths and lineNumbers for which results were found.
singleLineGrepStdoutToFilePathAndLineNr :: !String -> [(FilePath, Int)]

/**
 * Transforms the grep stdout which involves a surrounding line to a list of file paths and linenumbers.
 * E.g: when finding a constructor with a | or = on the preceding line 1 line
 * should be added to reach the constructor itself so the first argument should be 1.
 *
 * @param The predicate that should hold for the characters that are found before finding a stop symbol.
 * @param The stop symbols which should lead to short circuiting as the symbol which is searched for is found.
 * @param Whether the line should be reversed (check for stop symbol reading from end of line to front).
 * @param The stdout of grep.
 */
surroundingLineGrepStdoutToFilePathAndLineNr :: !(Char -> Bool) [!Char] !Bool !String -> [(FilePath, Int)]

//* A list of whitespace characters.
whitespaceChars :: [!Char]

//* A list of alphabetic and whitespace characters.
alphabeticAndWhitespaceChars :: [!Char]

/**
 * Converts a file and a line number to a location.
 * @param A tuple of a file path and a line number (1-based).
 * @result A location, or ?None when the file doesn't exist.
 */
fileAndLineToLocation :: !(!String, !Int) -> ?Location

/**
 * Wraps a request ID and a list of locations in a response message.
 * @param The request ID.
 * @param The list of locations.
 * @result A response message.
 */
locationResponse :: !RequestId ![!Location!] -> ResponseMessage

//* Not all characters belong to a searchable symbol. If a goto request is received for one of these "look-back"
//* characters, the parser scans the file backward from that position until it finds an actual symbol.
lookBackCharacters :: [!UChar]

//* Not all characters belong to a searchable symbol. If a goto request is received for one of these "look-forward"
//* characters, the parser scans the file forward from that position until it finds an actual symbol.
lookForwardCharacter :: UChar

//* Not all characters belong to a searchable symbol. If a goto request is received for one of these "special"
//* characters, Eastwood will not attempt to search.
//* {, } are not included because they can occur in a generic kind specification e.g: {|*|}.
isSpecialCharacter :: !UChar -> Bool

//* A regex matching at least one white space.
atleastOneWhiteSpace :: String

//* A regex matching any amount of white space.
anyAmountOfWhitespace :: String

//* A regex matching any amount of characters.
anyAmountOfCharacters :: String

//* A regex matching the start of a line.
lineStartsWith :: String

/** A predicate that indicates when to stop parsing.
 * @param The character.
 * @result The predicate.
 */
stopPredicate :: !UChar -> (UChar -> Bool)

//* A predicate that indicates when to stop parsing, that also includes symbols used within the generic kind
//* specification syntax.
stopPredicateAfterGenericKindSpecificationWasNotFound :: !UChar -> Bool

/**
 * Unless an infix function is being parsed this function removes the symbols surrounding the function from the string.
 * If an infix function is being parsed we remove the parenthesis surrounding the infix function if used prefix.
 *
 * @param the search term for which unwanted symbols should be removed.
 * @result the search term without the unwanted symbols.
 */
removeUnwantedSymbolsFromSearchTerm :: !String -> String

/**
 * Indicates if the search term is an infix symbol.
 * @param The search term.
 * @result A boolean indicating whether the search term is an infix symbol.
 */
isInfix :: !String -> Bool

/**
 * Parse backwards and forwards until a char that triggers the stopPredicate is found
 * or when there are no more characters to parse.
 * @param The stop predicate.
 * @param The line to retrieve the search term for.
 * @param The character number within the line that was selected.
 * @result The resulting search term.
 */
retrieveSearchTerm :: !(UChar -> Bool) !String !UInt -> String

/**
 * Indicates if a string starts with an upper case letter.
 * @param The string.
 * @result A boolean indicating whether the string starts with an upper case letter.
 */
startsWithUpper :: !String -> Bool

/** A grep search term for types.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepTypeSearchTerm :: !String -> String

/** A grep search term for functions.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepFuncSearchTerm :: !String -> String

/** A grep search term for generics.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepGenericSearchTerm :: !String -> String

/** A grep search term for classes.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepClassSearchTerm :: !String -> String

/** A grep search term for macros.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepMacroSearchTerm :: !String -> String

/** A grep search term for new or abstract types.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepNewOrAbstractTypeSearchTerm :: !String -> String

/** A grep search term for type synonyms.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepTypeSynonymSearchTerm :: !String -> String

/** A grep search term for constructors.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepConstructorSearchTerm :: !String -> String

/** A special case grep search term for constructors that have the | or = on the previous line.
 * @param The raw search term (with regex characters escaped).
 * @result The grep search term.
 */
grepConstructorSearchTermSpecialCase :: !String -> ?String

/**
 * This function escapes regex characters that can be found within the search term infix functions
 * E.g: a search term of ++| needs to become \+\+\| since otherwise the + and | have meaning in regex, causing the
 * grep search term to be misinterpreted.
 *
 * @param The raw search term.
 * @result The search term with the regex characters escaped.
 */
escapeRegexCharactersInSearchTerm :: !String -> String
