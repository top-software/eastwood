definition module Util

from Clean.Doc import :: ParseError, :: ParseWarning
from Data.Either import :: Either
from Data.Error import :: MaybeError
from Range import qualified :: Range, :: Position
from LSP.NotificationMessage import :: NotificationMessage
from LSP.Range import qualified :: Range
from LSP.RequestId import :: RequestId
from LSP.ResponseMessage import :: ResponseMessage, :: ErrorCode
from StdEnv import class toString
from System.FilePath import :: FilePath
from Text.URI import :: URI
from Text.YAML import :: YAMLErrorWithLocations
from _SystemStrictLists import class List

/**
 * Converts an Eastwood range to an LSP range.
 * @param The Eastwood range.
 * @result The LSP range.
 */
rangeCorrespondingTo :: !('Range'.Range 'Range'.Position) -> 'LSP.Range'.Range

instance toString YAMLErrorWithLocations

/**
 * A standard error response.
 * @param The request ID.
 * @param The error code.
 * @param The error message.
 * @result The error response.
 */
errorResponse :: !RequestId !ErrorCode !String -> ResponseMessage

/**
 * Find the search path containing a particular file.
 *
 * File system errors are ignored.
 *
 * NB: the context has `{#Char}` instead of `FilePath` due to
 * https://gitlab.science.ru.nl/clean-compiler-and-rts/compiler/-/issues/98.
 *
 * @param The relative file path to look for.
 * @param The list of search paths.
 * @result The first search path containing the file, if it could be found.
 */
findSearchPath :: !String !(l FilePath) !*World -> (!?FilePath, !*World) | List l {#Char}
	special l=[]; l=[!]

/**
 * @param The (url decoded) path of the file to resolve the module name for.
 * @param The search paths. This is needed for a final check. For instance,
 *   A/B/C.icl with the module name `C` may be fine if A/B is a search path,
 *   but if only A is a search path we would expect the module name `B.C`.
 * @result An error when the module name could not be parsed or does not match
 *   the file path / search paths; otherwise the parsed module name.
 */
resolveModuleName :: !FilePath ![FilePath] !*World -> (!MaybeError String String, !*World)

//* Converts an error string to a notification message indicating there is an error.
errorLogMessage :: !String -> NotificationMessage
