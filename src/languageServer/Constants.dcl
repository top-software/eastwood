definition module Constants

//* Relative path to the nitrile-packages folder containing the dependencies of this project.
NITRILE_PACKAGES_PATH :== "nitrile-packages"

//* Relative path to the linux-x64 folder containing the linux-x64 dependencies of this project.
LINUX_X64_PATH :== "linux-x64"

//* Relative path to the executables in the linux-x64 nitrile home directory.
EXE_PATH_REL :: String

//* The project configuration filename.
PROJECT_FILENAME :== "Eastwood.yml"

//* The URL to the Eastwood readme file.
README_LINK :== "https://gitlab.com/top-software/eastwood/-/blob/main/README.md"
